package com.devcamp.s50.task5840.restapi.model;

import java.util.Date;

import javax.persistence.*;


@Entity
@Table(name = "products")
public class CProducts {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "Ma_san_pham")
    private String maSanPham;

    @Column(name = "Ten_san_pham")
    private String tenSanPham;

    @Column(name = "Gia_tien")
    private double giaTien;

    @Column(name = "Ngay_tao")
    private Date ngayTao;

    @Column(name = "Ngay_cap_nhat")
    private Date ngayCapNhat;
    
    public CProducts() {
    }

    public CProducts(int id, String maSanPham, String tenSanPham, double giaTien, Date ngayTao, Date ngayCapNhat) {
        this.id = id;
        this.maSanPham = maSanPham;
        this.tenSanPham = tenSanPham;
        this.giaTien = giaTien;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public double getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(double giaTien) {
        this.giaTien = giaTien;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    

}
