package com.devcamp.s50.task5840.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5840.restapi.model.CProducts;

public interface IProducts extends JpaRepository<CProducts,Long> {
    
}
